This projet is a template structure to use when  starting a JEE projet.

To personnalize your app name, company, and version, you just have to modify the following lines in the pom.xml file.

<groupId>com.groupname</groupId>
<artifactId>appname</artifactId>
<version>1.0-SNAPSHOT</version>